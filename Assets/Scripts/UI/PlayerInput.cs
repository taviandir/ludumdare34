﻿using UnityEngine;
using Assets.Scripts.Units;
using System.Collections.Generic;

namespace Assets.Scripts.UI
{
    public class PlayerInput : MonoBehaviour
    {
        //private Player _player;

        public readonly float CamRayLength = 100f;
        private bool _isDragging;
        private bool _hasGrabbedBuilding;
        private Vector2 _dragStartPosition;
        private Vector2 _dragPossibleEndPosition;
        private List<Unit> selectedUnits;
        private string mouse1 = "Fire1";
        private string mouse2 = "Fire2";

        void Start()
        {
            //_player = GameManager.Player;
        }

        void FixedUpdate()
        {


            // TODO need UI collision that cancels all this
            // Mouse button 1 drag select
            if (Input.GetButtonDown(mouse1))
            {
                // 
                if (!_isDragging) { 
                    _dragStartPosition = Input.mousePosition;
                    _isDragging = true;
                }
                Rect selectionRect = new Rect(_dragStartPosition, Input.mousePosition);
            }
            else if (Input.GetButtonUp(mouse1))
            {
                _isDragging = false;
                Rect selectionRect = new Rect(_dragStartPosition, Input.mousePosition);

                foreach (Unit unit in ActorManager.Instance.Units) {
                    if (selectionRect.Contains(new Vector2(unit.transform.position.x, unit.transform.position.y)))
                    {
                        selectedUnits.Add(unit);
                    }
                }
            }

            if (Input.GetButtonDown(mouse1) && _hasGrabbedBuilding)
            {
                // Place occupiedBy at cursor
            }

            // Mouse button 2 Move or Attack 
            if (Input.GetButtonDown(mouse2))
            {
                foreach (Unit unit in selectedUnits)
                {
                    GameObject mouseHitObject = MouseHit();
                    TileNode hitTileNode = mouseHitObject.GetComponent<TileNode>();
                    if (hitTileNode != null) {
                        unit.OrderMoveTo(hitTileNode);
                    }
                    // TODO test if occupiedBy enemy
                    Units.Structure hitStructure = mouseHitObject.GetComponent<Units.Structure>();

                }
            }
        }

        private GameObject MouseHit()
        {
            // Ray cast from main camera
            Vector2 screenToWorldPoint = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
            RaycastHit2D rayHit = Physics2D.Raycast(screenToWorldPoint, Vector2.zero, 0f);

            if (rayHit) // TODO check if it works without mask
            {
                GameObject sceneObjectHitByRay = rayHit.collider.gameObject;
                Vector2 sceneObjectPosition = rayHit.collider.gameObject.transform.position;
                return sceneObjectHitByRay;
                //var useable = sceneObjectHitByRay.GetComponent<IUseable>();

                //    if (useable != null)
                //        _player.MoveToThenUse(sceneObjectPosition, useable);
                //    else if (Vector3.Magnitude(rayHit.point - _player.transform.position) >= MinDeltaDestination)
                //    {
                //        _player.StopCoroutine("UseWhenPossible");
                //        _player.SetDestination(rayHit.point);
                //    }
                //}
            }
            return null;
        }
    }
}
