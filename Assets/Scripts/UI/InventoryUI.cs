﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.UI
{
    abstract class InventoryUI : MonoBehaviour
    {
        public GameObject SlotPrefab;
        //public Inventory Inventory;
        public int NumSlots; // temp
        private List<Vector3> _slotsLocalPosition;

        //public Item GrabbedItem;

        void Start()
        {
            //GrabbedItem = GameManager.Player.GrabbedItem;
        }

        protected void CreateGUI(int columns, float spacing)
        {
            var slotRectTransform = SlotPrefab.GetComponent<RectTransform>();
            _slotsLocalPosition = new List<Vector3>();

            for (int i = 0; i != NumSlots; ++i)
            {
                var prevSlot = Instantiate(SlotPrefab);
                prevSlot.transform.SetParent(gameObject.transform);
                float x = i * slotRectTransform.rect.width + spacing;
                _slotsLocalPosition.Add(slotRectTransform.localPosition + new Vector3(x, 0, 0));
                prevSlot.transform.localPosition = _slotsLocalPosition[i];

                var prevSlotUI = prevSlot.GetComponent<SlotUI>();

                prevSlotUI.SlotIndex = i;

                // TODO place buildings into slots in this loop


                //prevSlotUI.Inventory = Inventory;


                //                lastSlot.GetComponent<RectTransform>().localPosition = slotRect.localPosition;
                //                lastSlot.GetComponent<RectTransform>().position = slotRect.position;
            }
        }

        private void ItemAtMouse(Sprite itemIcon)
        {
            //Input.mousePosition
        }
    }
}
