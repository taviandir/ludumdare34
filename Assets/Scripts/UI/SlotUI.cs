﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Scripts.UI
{
    class SlotUI : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public int SlotIndex;
        //public Inventory Inventory;
        public bool IsPointerOverSlot;

       // private Player _player;

        void Start()
        {
            //_player = GameManager.Player;

            
        }

        void FixedUpdate()
        {
            //update item image of slot

            if (IsPointerOverSlot)
            {
                PlayerInput();
            }
        }

        public void PlayerInput()
        {
            if (Input.GetButton("Fire1"))
            {
                // Start building unit or Grab building from UI
            }

            if (Input.GetButton("Fire2"))
            {
                // Do nothing?
            }
        }

        private void ShowTooltip()
        {
            //if (Inventory.InventoryItems[SlotIndex] == Item.Empty)
            //    return;
            //Debug.Log(Inventory.InventoryItems[SlotIndex].Description);
        }

        private void HideTooltip()
        {
            //if (Inventory.InventoryItems[SlotIndex] == Item.Empty)
            //    return;
            //Debug.Log("hide description");
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            IsPointerOverSlot = true;
            ShowTooltip();
        }
        public void OnPointerExit(PointerEventData eventData)
        {
            IsPointerOverSlot = false;
            HideTooltip();
        }
    }
}
