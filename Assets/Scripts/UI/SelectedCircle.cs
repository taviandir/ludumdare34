﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class SelectedCircle : MonoBehaviour
{
    public LineRenderer LineRenderer;
    public int Points = 30;
    public float Distance = 0.8f;

    private const int _zValue = -1;

    void Start()
    {
        //SetCircle(this.pos()); // testing
    }

    public void SetCircle(Vector3 worldPos)
    {
        worldPos.z = _zValue;
        this.gameObject.SetActive(true);
        LineRenderer.SetVertexCount(30);
        LineRenderer.useWorldSpace = true;

        float anglePerPoint = 360f/Points;
        for (int i = 0; i < Points - 1; ++i)
        {
            float angle = i * anglePerPoint; // ;
            var pos = MathUtilities.CalculatePosition(angle * Mathf.Deg2Rad, Distance);
            LineRenderer.SetPosition(i, pos + worldPos);
        }

        // last pos same as first
        var startPos = MathUtilities.CalculatePosition(0, Distance);
        LineRenderer.SetPosition(Points - 1, startPos + worldPos);
    }

    public void UnsetCircle()
    {
        this.gameObject.SetActive(false);
    }
}