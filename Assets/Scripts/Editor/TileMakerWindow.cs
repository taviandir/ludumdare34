﻿using UnityEditor;
using UnityEngine;

public class TileMakerWindow : EditorWindow
{
    private TileMaker tileMaker;

    public void Init(TileMaker tileMaker)
    {
        this.tileMaker = tileMaker;
        //tileMaker = (TileMaker) FindObjectOfType<TileMaker>();
    }

    public void OnGUI()
    {
        tileMaker.gridColor = EditorGUILayout.ColorField(tileMaker.gridColor, GUILayout.Width(200));
        tileMaker.connectionColor = EditorGUILayout.ColorField(tileMaker.connectionColor, GUILayout.Width(200));
    }
}
