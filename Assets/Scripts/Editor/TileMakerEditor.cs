﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Units;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TileMaker))]
public class TileMakerEditor : Editor
{
    private TileMaker tileMaker;

    void OnEnable()
    {
        tileMaker = (TileMaker)target;
    }

    [MenuItem("Assets/Create/TileSet")]
    public static void CreateTileSet()
    {
        TileSet asset = ScriptableObject.CreateInstance<TileSet>();
        AssetDatabase.CreateAsset(asset, "Assets/TileSet.asset");
        AssetDatabase.SaveAssets();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
        asset.hideFlags = HideFlags.DontSave;
    }

    [MenuItem("Assets/Create/TileStructureSet")]
    public static void CreateTileStructureSet()
    {
        TileStructureSet asset = ScriptableObject.CreateInstance<TileStructureSet>();
        AssetDatabase.CreateAsset(asset, "Assets/TileStructureSet.asset");
        AssetDatabase.SaveAssets();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
        asset.hideFlags = HideFlags.DontSave;
    }

    public override void OnInspectorGUI()
    {
        int index = 0;
        // UNCOMMENT THIS TO SEE AND ADJUST GRID SIZE MANUALLY
        //tileMaker.width = CreateSlider("Grid Width", tileMaker.width);
        //tileMaker.height = CreateSlider("Grid Height", tileMaker.height);
        //SceneView.RepaintAll();

        if (GUILayout.Button("Change Grid Color"))
        {
            TileMakerWindow window = EditorWindow.GetWindow<TileMakerWindow>();
            window.Init(tileMaker);
        }
        if (GUILayout.Button("Update Connections"))
        {
            UpdatePathfindingConnections();
            SceneView.RepaintAll();
        }

        // Show Grid
        tileMaker.drawGrid = EditorGUILayout.Toggle("Show Grid", tileMaker.drawGrid);

        // Show Connections
        tileMaker.drawConnections = EditorGUILayout.Toggle("Show Connections", tileMaker.drawConnections);

        // Pathfinding: Corner cuttin
        tileMaker.cornerCutting = EditorGUILayout.Toggle("Corner Cutting", tileMaker.cornerCutting);

        // Tile Prefab
        EditorGUI.BeginChangeCheck();
        var newTilePrefab =
            (Transform) EditorGUILayout.ObjectField("Selected prefab", tileMaker.selectedPrefab, typeof (Transform), false);
        if (EditorGUI.EndChangeCheck())
        {
            tileMaker.selectedPrefab = newTilePrefab;
            Undo.RecordObject(target, "TileMaker Changed"); // = allows the user to undo to revert the change
        }

        // Tile Map
        EditorGUI.BeginChangeCheck();
        var newTileSet = (TileSet)EditorGUILayout.ObjectField("Tileset", tileMaker.tileSet, typeof (TileSet), false);
        if (EditorGUI.EndChangeCheck())
        {
            tileMaker.tileSet = newTileSet;
            Undo.RecordObject(target, "TileMaker Changed");
        }

        // Tile Map
        EditorGUI.BeginChangeCheck();
        var newTileStructureSet = (TileStructureSet)EditorGUILayout.ObjectField("Structure set", tileMaker.structureSet, typeof(TileStructureSet), false);
        if (EditorGUI.EndChangeCheck())
        {
            tileMaker.structureSet = newTileStructureSet;
            Undo.RecordObject(target, "TileMaker Changed");
        }

        // Layer selection
        var layerNames = Enum.GetNames(typeof (TileLayer));
        var valuesArr = Enum.GetValues(typeof (TileLayer));
        int[] layerValues = new int[valuesArr.GetLength(0)];
        for (int i = 0; i < valuesArr.GetLength(0); ++i)
        {
            layerValues[i] = (int)valuesArr.GetValue(i);
        }
        EditorGUI.BeginChangeCheck();
        index = EditorGUILayout.IntPopup("Select layer", tileMaker.layerSelectionIndex, layerNames, layerValues);
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(target, "TileMaker Changed");
            if (tileMaker.layerSelectionIndex != index)
            {
                tileMaker.layerSelectionIndex = index;
                tileMaker.currentLayer = (TileLayer)index;

                // select prefab from new layer
                if (tileMaker.currentLayer == TileLayer.Map)
                {
                    tileMaker.selectedPrefab = tileMaker.tileSet.prefabs[tileMaker.tileSelectionIndex].transform;
                }
                else if (tileMaker.currentLayer == TileLayer.Structures)
                {
                    tileMaker.selectedPrefab = tileMaker.structureSet.prefabs[tileMaker.structureSelectionIndex].transform;
                }
            }
        }

        // Tile Set (Map layer)
        if (tileMaker.currentLayer == TileLayer.Map && tileMaker.tileSet != null)
        {
            var names = new string[tileMaker.tileSet.prefabs.Count];
            var values = new int[tileMaker.tileSet.prefabs.Count];

            for (int i = 0; i < names.Length; ++i)
            {
                names[i] = tileMaker.tileSet.prefabs[i] != null ? tileMaker.tileSet.prefabs[i].name : "";
                values[i] = i;
            }

            EditorGUI.BeginChangeCheck();
            index = EditorGUILayout.IntPopup("Select tile", tileMaker.tileSelectionIndex, names, values);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(target, "TileMaker Changed");
                if (tileMaker.tileSelectionIndex != index)
                {
                    tileMaker.tileSelectionIndex = index;
                    tileMaker.selectedPrefab = tileMaker.tileSet.prefabs[index].transform;

                    SceneView.RepaintAll();
                }
            }
        }
        // Tile Set (Structure layer)
        if (tileMaker.currentLayer == TileLayer.Structures && tileMaker.structureSet != null)
        {
            var names = new string[tileMaker.structureSet.prefabs.Count];
            var values = new int[tileMaker.structureSet.prefabs.Count];

            for (int i = 0; i < names.Length; ++i)
            {
                names[i] = tileMaker.structureSet.prefabs[i] != null ? tileMaker.structureSet.prefabs[i].name : "";
                values[i] = i;
            }

            EditorGUI.BeginChangeCheck();
            index = EditorGUILayout.IntPopup("Select occupiedBy", tileMaker.structureSelectionIndex, names, values);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(target, "TileMaker Changed");
                if (tileMaker.structureSelectionIndex != index)
                {
                    tileMaker.structureSelectionIndex = index;
                    tileMaker.selectedPrefab = tileMaker.structureSet.prefabs[index].transform;

                    SceneView.RepaintAll();
                }
            }
        }
    }

    private void UpdatePathfindingConnections()
    {
        // Step 1: Clear current connections
        foreach (Transform child in tileMaker.transform)
        {
            var node = child.GetComponent<TileNode>();
            if (node != null)
            {
                node.ClearConnections();
                EditorUtility.SetDirty(node);
            }
        }
        // Step 2: Calculate new connections
        int i = 0;
        while (i < tileMaker.transform.childCount)
        {
            Transform t = tileMaker.transform.GetChild(i++);
            TileNode node = t.GetComponent<TileNode>();
            if (node == null || !node.isWalkable)
                continue; // we don't wanna create connections with unwalkable tiles
            TileNode nodeAbove = tileMaker.GetTileNodeAtCoordinate(node.xCoord, node.yCoord + 1);
            TileNode nodeDiagonalUp = tileMaker.GetTileNodeAtCoordinate(node.X + 1, node.Y + 1);
            TileNode nodeRight = tileMaker.GetTileNodeAtCoordinate(node.X + 1, node.Y);
            TileNode nodeDiagonalDown = tileMaker.GetTileNodeAtCoordinate(node.X + 1, node.Y - 1);
            TileNode nodeBelow = tileMaker.GetTileNodeAtCoordinate(node.X, node.Y - 1);
            if (nodeAbove != null && nodeAbove.isWalkable)
            {
                node.AddConnection(nodeAbove);
                nodeAbove.AddConnection(node);
            }
            if (nodeDiagonalUp != null && nodeDiagonalUp.isWalkable && nodeAbove != null && nodeRight != null)
            {
                if ((tileMaker.cornerCutting && (nodeAbove.isWalkable || nodeRight.isWalkable)) ||
                    (!tileMaker.cornerCutting && (nodeAbove.isWalkable && nodeRight.isWalkable)))
                {
                    node.AddConnection(nodeDiagonalUp);
                    nodeDiagonalUp.AddConnection(node);
                }
            }
            if (nodeRight != null && nodeRight.isWalkable)
            {
                node.AddConnection(nodeRight);
                nodeRight.AddConnection(node);
            }
            if (nodeDiagonalDown != null && nodeDiagonalDown.isWalkable && nodeRight != null && nodeBelow != null)
            {
                if ((tileMaker.cornerCutting && (nodeRight.isWalkable || nodeBelow.isWalkable)) ||
                    (!tileMaker.cornerCutting && (nodeRight.isWalkable && nodeBelow.isWalkable)))
                {
                    node.AddConnection(nodeDiagonalDown);
                    nodeDiagonalDown.AddConnection(node);
                }
            }


            // NOT NECESSARY
            //if (nodeBelow.isWalkable)
            //{
            //    node.AddConnection(nodeBelow);
            //    nodeBelow.AddConnection(node);
            //}
        } // end of while
        // Step 3: Save
        foreach (Transform child in tileMaker.transform)
        {
            var node = child.GetComponent<TileNode>();
            EditorUtility.SetDirty(node);
            //EditorUtility.SetDirty(nodeAbove);
            //EditorUtility.SetDirty(nodeDiagonalUp);
            //EditorUtility.SetDirty(nodeRight);
            //EditorUtility.SetDirty(nodeDiagonalUp);
            //EditorUtility.SetDirty(nodeBelow);
        }
        Debug.Log("[TileMaker]: Connections updated.");
    }

    private float CreateSlider(string labelName, float sliderPosition)
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label(labelName);
        sliderPosition = EditorGUILayout.Slider(sliderPosition, 1f, 100f);
        GUILayout.EndHorizontal();
        return sliderPosition;
    }

    void OnSceneGUI()
    {
        int controlId = GUIUtility.GetControlID(FocusType.Passive);
        Event e = Event.current;

        if (e.isMouse && e.type == EventType.MouseDown && e.button == 0)
        {
            GUIUtility.hotControl = controlId;
            e.Use();
            GameObject gameObject;
            Transform prefab = tileMaker.selectedPrefab;
            if (tileMaker.currentLayer == TileLayer.Map)
            {
                if (prefab && CanPlaceTile(e))
                {
                    Undo.IncrementCurrentGroup();
                    gameObject = PlaceTile(e);
                    Undo.RegisterCreatedObjectUndo(gameObject, "Create " + gameObject.name);
                }
            }
            else if (tileMaker.currentLayer == TileLayer.Structures)
            {
                if (prefab)
                {
                    var ts = prefab.GetComponent<Structure>();
                    List<TileNode> nodes;
                    if (CanPlaceStructure(e, ts, out nodes))
                    {
                        Undo.IncrementCurrentGroup();
                        GameObject structure = PlaceStructure(e, nodes);
                        Undo.RegisterCreatedObjectUndo(structure, "Create " + structure.name);
                    }
                    else
                        Debug.Log("[TM] Cant place occupiedBy there");
                }
            }
        }
        else if (e.isMouse && e.type == EventType.MouseDown && e.button == 1)
        {
            GUIUtility.hotControl = controlId;
            e.Use();
            if (tileMaker.currentLayer == TileLayer.Map)
                DeleteTile(e);
            else if (tileMaker.currentLayer == TileLayer.Structures)
                DeleteStructure(e);
        }
        else if (e.isMouse && e.type == EventType.MouseUp && (e.button == 0 || e.button == 1))
        {
            GUIUtility.hotControl = 0;
        }
    }

    private bool CanPlaceTile(Event e)
    {
        TileCoord coord = GetCoordinatesFromMousePosition(e);
        TileNode node = tileMaker.GetTileNodeAtCoordinate(coord);
        return node == null;
    }

    private bool CanPlaceStructure(Event e, Structure str, out List<TileNode> nodes)
    {
        nodes = new List<TileNode>(str.SizeX * str.SizeY);
        TileCoord initialCoord = GetCoordinatesFromMousePosition(e);
        for (int x = 0; x < str.SizeX; ++x)
        {
            for (int y = 0; y < str.SizeY; ++y)
            {
                var node = tileMaker.GetTileNodeAtCoordinate(initialCoord.X + x, initialCoord.Y + y);
                nodes.Add(node);
            }
        }

        return nodes.All(n => n.isWalkable && n.occupiedBy == null);
    }

    private GameObject PlaceTile(Event e)
    {
        GameObject go = (GameObject)PrefabUtility.InstantiatePrefab(tileMaker.selectedPrefab.gameObject);
        Vector3 aligned = GetAlignedPosition(e);
        go.transform.position = aligned;
        go.transform.parent = tileMaker.transform;
        TileCoord coord = GetCoordinatesFromMousePosition(e);
        var tileNode = go.GetComponent<TileNode>();
        go.name += string.Format("[{0},{1}]", coord.X, coord.Y);
        tileNode.SetCoords(coord);
        return go;
    }

    private GameObject PlaceStructure(Event e, List<TileNode> affectedNodes)
    {
        GameObject go = (GameObject)PrefabUtility.InstantiatePrefab(tileMaker.selectedPrefab.gameObject);
        Vector3 aligned = GetAlignedPosition(e);
        var str = go.GetComponent<Structure>();

        // calculate offset
        float xMid = str.SizeX/2.0f;
        float yMid = str.SizeY/2.0f;
        float xOffsetDiff = Mathf.Abs(0.5f - xMid);
        float yOffsetDiff = Mathf.Abs(0.5f - yMid);
        float xOffset = tileMaker.width*xOffsetDiff;
        float yOffset = tileMaker.height*yOffsetDiff;

        // set position
        go.transform.position = aligned + new Vector3(xOffset, yOffset);
        var am = ActorManager.Instance;
        go.transform.parent = am.StructureParent;
        am.FriendlyStructures.Add(str);

        tileMaker.SetBuiltTile(str, affectedNodes);
        str.Occupies = affectedNodes; // save which nodes it is saved upon
        return go;
    }

    private void DeleteTile(Event e)
    {
        Vector3 aligned = GetAlignedPosition(e);
        Transform tt = GetTransformFromPosition(aligned);
        if (tt != null)
        {
            DestroyImmediate(tt.gameObject);
        }
    }

    private void DeleteStructure(Event e)
    {
        Debug.Log("DeleteStructure()");
        TileCoord tc = GetCoordinatesFromMousePosition(e);
        TileNode tn = tileMaker.GetTileNodeAtCoordinate(tc);
        if (tn.occupiedBy != null)
        {
            Structure ts = tn.occupiedBy.GetComponent<Structure>();
            tileMaker.UnsetBuiltTile(ts.Occupies); // reset tilenode
            DestroyImmediate(ts.gameObject);
        }
    }

    private Vector3 GetAlignedPosition(Event e)
    {
        Ray ray =
            Camera.current.ScreenPointToRay(new Vector3(e.mousePosition.x,
                -e.mousePosition.y + Camera.current.pixelHeight, 0));
        Vector3 mousePos = ray.origin;
        return new Vector3(Mathf.Floor(mousePos.x/tileMaker.width)*tileMaker.width + tileMaker.width/2.0f,
            Mathf.Floor(mousePos.y/tileMaker.height)*tileMaker.height + tileMaker.height/2.0f, 0.0f);
    }

    private TileCoord GetCoordinatesFromMousePosition(Event e)
    {
        Ray ray =
            Camera.current.ScreenPointToRay(new Vector3(e.mousePosition.x,
                -e.mousePosition.y + Camera.current.pixelHeight, 0));
        Vector3 mousePos = ray.origin;
        return new TileCoord(Mathf.FloorToInt(mousePos.x / tileMaker.width), Mathf.FloorToInt(mousePos.y / tileMaker.height));
    }

    private Transform GetTransformFromPosition(Vector3 aligned)
    {
        int i = 0;
        while (i < tileMaker.transform.childCount)
        {
            Transform t = tileMaker.transform.GetChild(i++);
            if (t.position == aligned)
            {
                return t;
            }
        }
        return null;
    }
}
