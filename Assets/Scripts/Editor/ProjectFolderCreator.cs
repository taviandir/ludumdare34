/* 
 * Created:         2013-06-20
 * Last updated:    2013-06-20
 * Description:     
 * Comment:         
 * 
 ************************************
 *
 * Imports:
 * 
 */

using UnityEditor;
using UnityEngine;
using System.IO;

public class ProjectFolderCreator : MonoBehaviour 
{
    /// <summary>
    /// Menu item to create folders
    /// </summary>
    [MenuItem("Project Tools/Create Folders")]
    static void CreateFoldersMenuItem()
    {
        CreateFolders();
    }

    /// <summary>
    /// Generates project folder items specified in the "folders.txt" file located in the root folder of the project.
    /// If the file doesn't exist, no folders will be created.
    /// </summary>
    private static void CreateFolders()
    {
        if (File.Exists("folders.txt"))
        {
            string projectPath = Application.dataPath + "/";
            
            using (StreamReader sr = new StreamReader("folders.txt"))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    Directory.CreateDirectory(projectPath + line);
                }
            }
            AssetDatabase.Refresh();
        }
    }
}
