﻿using UnityEditor;
using UnityEngine;

public class EditorTextPrompt : EditorWindow
{
    private string textValue = "";
    private bool overwrite = false;

    [MenuItem("Assets/Create/Scriptable Object")]
    static void Init()
    {
        var window = GetWindow<EditorTextPrompt>();
        window.position = new Rect(Screen.width / 2 - 100, Screen.height / 2 - 20, 200, 80); // 80 seems to be the minimum height
        window.titleContent = new GUIContent("Create new");
        window.ShowUtility();
    }

    void OnGUI()
    {
        try
        {
            GUILayout.BeginArea(new Rect(0, 0, 200, 80));
            GUILayout.BeginVertical();
            GUI.SetNextControlName("InputField");
            textValue = GUILayout.TextField(textValue);
            overwrite = GUILayout.Toggle(overwrite, "Overwrite if exists?");
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("OK"))
            {
                CreateAndClose();
            }
            if (GUILayout.Button("Cancel"))
            {
                this.Close();
            }
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUILayout.EndArea();
            GUI.FocusControl("InputField");

            // react on enter
            Event e = Event.current;
            if (e.type == EventType.keyUp && e.keyCode == KeyCode.Return && EditorWindow.focusedWindow == this)
            {
                Debug.Log("enter received");
                CreateAndClose();
            }
        }
        catch {}
    }

    private void CreateAndClose()
    {
        this.Close();
        CreateScriptableObject.InitAsset(textValue, overwrite);
    }
}
