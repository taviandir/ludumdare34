﻿using UnityEngine;

[System.Serializable]
public class MyScriptableObjectClass : ScriptableObject
{
    public bool lovesProgramming;
    public int[] numbersArray;
    public int age;
    public string lifeQuote;
    public NestedClass nested;
}

[System.Serializable]
public class NestedClass
{
    public string something;
    public Vector3[] coordinates;
}