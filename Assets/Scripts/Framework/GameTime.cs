﻿/* 
 * Created:         2013-09-02
 * Last updated:    2013-09-02
 * Description:     A wrapper class for the time system in Unity. 
 * Comment:         By using this method instead of the native values from Unity's Time class, some pausing issues are resolved.
 * 
 ************************************
 *
 * Imports:
 * 
 */

using UnityEngine;
using System.Linq;
using System.Collections;

public class GameTime : MonoBehaviour
{
    public static float deltaTime;        // time since last frame
    public static float time;             // time spent on this level
    public static float totalTime;        // time spent on game session
    public static bool isPaused;
    public static float timeScale;
    
    private static float timeScaleBeforePause;

    void Start()
    {
        deltaTime = 0.0f;
        time = 0.0f;
        totalTime = 0.0f;
        timeScale = Time.timeScale;
    }

    void Update()
    {
        UpdateGameTime();
    }

    void OnLevelWasLoaded(int level)
    {
        Object.DontDestroyOnLoad(this.gameObject); // ToDo : change this, maek the class static and have another script call UpdateGameTime() instead?
        ResetGameTime();
    }

    private void ResetGameTime()
    {
        deltaTime = 0.0f;
        time = 0.0f;
    }

    public void UpdateGameTime()
    {
        // double-check if other scripts have regulated timeScale (even tho they shouldnt)
        if (!isPaused && Time.timeScale == 0)
            Pause();
        else if (isPaused && Time.timeScale > 0)
            Unpause();


        if (!isPaused)
        {
            Time.timeScale = timeScale;
            deltaTime += Time.deltaTime * timeScale; // should this multiply?
            time += deltaTime;
            totalTime += deltaTime;
        }
        else // if (isPaused)
        {
            //ToDo: should something be done here?
        }
    }

    /// <summary>
    /// Pauses the game.
    /// </summary>
    public static void Pause()
    {
        if (!isPaused)
        {
            timeScaleBeforePause = Time.timeScale;
            Time.timeScale = 0;
            isPaused = true;
        }
    }

    /// <summary>
    /// Unpauses the game. Continues with the time scale value that was left before pausing.
    /// </summary>
    public static void Unpause()
    {
        if (isPaused)
        {
            isPaused = false;
            if (timeScaleBeforePause > 0) // != 0 ? this way stuff can go in "reverse time"
                Time.timeScale = timeScale = timeScaleBeforePause;
            else
                Time.timeScale = timeScale = 1;
        }
    }
}
