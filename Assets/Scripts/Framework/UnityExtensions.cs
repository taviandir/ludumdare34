/* 
 * Created:         2013-01-01
 * Last updated:    2013-01-01
 * Description:     
 * Comment:         
 * 
 ************************************
 *
 * Imports:
 * 
 */

using UnityEngine;
using System.Linq;
using System.Collections;

public static class UnityExtensions
{
    public static float x(this Transform t)
    {
        return t.position.x;
    }

    public static float y(this Transform t)
    {
        return t.position.y;
    }

    public static float z(this Transform t)
    {
        return t.position.z;
    }

    public static Vector3 pos(this Transform t)
    {
        return t.position;
    }

    public static float x(this MonoBehaviour t)
    {
        return t.transform.position.x;
    }

    public static float y(this MonoBehaviour t)
    {
        return t.transform.position.y;
    }

    public static float z(this MonoBehaviour t)
    {
        return t.transform.position.z;
    }

    public static Vector3 pos(this MonoBehaviour t)
    {
        return t.transform.position;
    }

    public static float x(this GameObject t)
    {
        return t.transform.position.x;
    }

    public static float y(this GameObject t)
    {
        return t.transform.position.y;
    }

    public static float z(this GameObject t)
    {
        return t.transform.position.z;
    }

    public static Vector3 pos(this GameObject t)
    {
        return t.transform.position;
    }

    public static GameObject go(this Transform t)
    {
        return t.gameObject;
    }

    public static bool isWithin(this GameObject go, GameObject target, float range, RangeMode axis)
    {
        Vector3 diff = go.pos() - target.pos();

        float realDiff = 0;
        switch (axis)
        {
            case RangeMode.X:
                realDiff = Mathf.Abs(diff.x);
                break;
            case RangeMode.Y:
                realDiff = Mathf.Abs(diff.y);
                break;
            case RangeMode.Z:
                realDiff = Mathf.Abs(diff.z);
                break;
            case RangeMode.Distance:
                realDiff = Vector3.Distance(go.pos(), target.pos());
                break;
        }

        if (realDiff < range)
            return true;
        else
            return false;
    }

    public static Vector3 ScreenToViewPoint(this Vector3 pos)
    {
        return new Vector3(pos.x / (1.0f * Screen.width), pos.y / (1.0f * Screen.height));
    }
}


public enum RangeMode
{
    X,
    Y,
    Z,
    Distance
}