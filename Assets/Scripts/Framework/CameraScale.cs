﻿using UnityEngine;

/// <summary>
/// For pixel perfect camera.
/// </summary>
public class CameraScale : MonoBehaviour
{
    public bool active = true;
    public float floatable = 100.0f;
        //This can be PixelsPerUnit, or you can change it during runtime to alter the camera.

    private void Awake()
    {
        if (!active)
            return;
        GetComponent<Camera>().orthographicSize = Screen.height*gameObject.GetComponent<Camera>().rect.height/floatable/2.0f; //- 0.1f;
    }
}
