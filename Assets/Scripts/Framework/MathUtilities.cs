﻿using UnityEngine;
using System;
using Assets.Scripts.Framework;

// MATH!  (╯°□°）╯︵ ┻━┻
public sealed class MathUtilities 
{
	public static Vector3 CalculatePosition(Vector3 positionOne, Vector3 positionTwo, float distance, bool inverseAngle = false)
	{
	    float angle = GetAngle(positionOne, positionTwo) + (inverseAngle ? 180 * Mathf.Deg2Rad : 0);
		float xValue = Convert.ToSingle(Math.Round(Math.Cos(angle) * distance, 2));
		float yValue = Convert.ToSingle(Math.Round(Math.Sin(angle) * distance, 2));
		return new Vector3(xValue, yValue, 0);
	}

	public static Vector3 CalculatePosition(Vector3 positionOne, Vector3 positionTwo, float distance, out float angle, bool inverseAngle = false)
	{
        angle = GetAngle(positionOne, positionTwo) + (inverseAngle ? 180 * Mathf.Deg2Rad : 0);
		float xValue = Convert.ToSingle(Math.Round(Math.Cos(angle) * distance, 2));
		float yValue = Convert.ToSingle(Math.Round(Math.Sin(angle) * distance, 2));
		return new Vector3(xValue, yValue, 0);
	}

	public static Vector3 CalculatePosition(float angle, float distance)
	{
		float xValue = Convert.ToSingle(Math.Round(Math.Cos(angle) * distance, 2));
		float yValue = Convert.ToSingle(Math.Round(Math.Sin(angle) * distance, 2));
		return new Vector3(xValue, yValue, 0);
	}

    public static double ManhattanDistance(IPoint p1, IPoint p2)
    {
        return (Math.Abs(p1.X - p2.X) + Math.Abs(p1.Y - p2.Y));
    }

    /// <summary>
    /// Gets the virtual angle between two points, assuming with horizontal lines.
    /// </summary>
    /// <returns>The angle in radians.</returns>
    /// <param name="from">From.</param>
    /// <param name="to">To.</param>
    public static float GetAngle(Vector2 from, Vector2 to)
	{
		float angle = Mathf.Atan2(to.y - from.y, to.x - from.x);
		return angle; 
	}
}
