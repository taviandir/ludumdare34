﻿/* 
 * Created:         2013-01-01
 * Last updated:    2013-01-01
 * Description:     
 * Comment:         
 * 
 ************************************
 *
 * Imports:
 * 
 */

using UnityEngine;
using System.Linq;
using System.Collections;

public static class GUIx
{
    /// <summary>
    /// Draws a Label with (what looks like) an outline around the characters. What it actually does is that it draws the same label 4 times around and underneath the actual text.
    /// The color of the text will be what the 'GUIStyle.normal.textColor' property has as value. If 'outlineSize' value is 0, it will act as a normal 'GUI.Label()'.
    /// </summary>
    /// <param name="rect">Position and size of the Label.</param>
    /// <param name="text">The text to be presented.</param>
    /// <param name="style">The GUIStyle for the text.</param>
    /// <param name="outColor">The outline color.</param>
    /// <param name="outlineSize">Optional parameter. This value should almost always be 1 (default). Should never exceed 3.</param>
    public static void LabelOutline(Rect rect, string text, GUIStyle style, Color outColor, int outlineSize = 1)
    {
        if (outlineSize >= 0) // if outlineSize has a positive value, do the outlining
        {
            Color inColor = style.normal.textColor;
            style.normal.textColor = outColor;

            // draw left outline
            rect.x -= (1 * outlineSize);
            GUI.Label(rect, text, style);

            // draw right outline
            rect.x += (2 * outlineSize);
            GUI.Label(rect, text, style);

            // draw top outline
            rect.x -= (1 * outlineSize);
            rect.y -= (1 * outlineSize);
            GUI.Label(rect, text, style);

            // draw bottom outline
            rect.y += (2 * outlineSize);
            GUI.Label(rect, text, style);

            //revert back to original position
            rect.y -= (1 * outlineSize);

            // change font color and draw actual text
            style.normal.textColor = inColor;
        }

        GUI.Label(rect, text, style);
    }
}
