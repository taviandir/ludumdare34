﻿/* 
 * Created:         2013-01-01
 * Last updated:    2013-01-01
 * Description:     
 * Comment:         
 * 
 ************************************
 *
 * Imports:
 * 
 */

using UnityEngine;
using System.Linq;
using System.Collections;
using System;
using System.Text;

public sealed class GConsole
{
    /// <summary>
    /// Just like Debug.Log(), except this method includes a timestamp isnerted prior to the message.
    /// </summary>
    /// <param name="obj">Message to be sent, or object to be presented as a message.</param>
    public static void Line(object obj)
    {
        float f = Time.time;
        TimeSpan span = TimeSpan.FromSeconds(f);

        StringBuilder sb = new StringBuilder();
        sb.Append("[");
        //sb.Append(span.Hours.ToString() + ":");
        sb.Append(span.Minutes.ToString() + ":");
        sb.Append(span.Seconds.ToString() + ".");
        sb.Append(span.Milliseconds.ToString());
        sb.Append("]");

        sb.Append(obj.ToString());

        Debug.Log(sb.ToString());
    }

    /// <summary>
    /// Alternative name for the Line() method. Calls the Line method.
    /// </summary>
    /// <param name="obj">Message to be sent, or object to be presented as a message.</param>
    public static void Log(object obj)
    {
        Line(obj);
    }
}
