﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Assets.Scripts.Framework
{
    #region Interfaces
    public interface IPoint
    {
        int X { get; }
        int Y { get; }
        //public boolean IsPassable; TODO
    }
    #endregion
    public abstract class AStarAlgorithm<TPoint> where TPoint : IPoint
    {
        #region Instance Variables
        protected int iter;
        #endregion

        #region Constructors
        protected AStarAlgorithm(){ }
        #endregion
        
        #region Methods
        // Finds a minimal path (as long as heuristic assigned to this instance is admissible) 
        // between startPoint and endPoint through a set of points (see above for interface), 
        // TPointSet, making use of the A* algorithm with the heuristic, Heuristic.
        public IEnumerable<TPoint> FindPathBetween(TPoint startPoint, TPoint endPoint)
        {
            // Solution stored as a stack because later we will recurse backwards on the
            // path and we want the returned IEnumerable to give the path from startPoint
            // to endPoint
            var solution = new Stack<TPoint>();
            
            // if the startPoint has same coordinates as endPoint, give endPoint
            if (startPoint.X == endPoint.X && startPoint.Y == endPoint.Y)
            {
                solution.Push(endPoint);
                return solution;
            }

            if (!IsSolvable(startPoint, endPoint))
                throw new ArgumentException("A* algorithm's method isSolvable has returned " +
                                            "false, pathfinding problem not solvable");

            #region Core of A* algorithm

            var closedSet = new HashSet<TPoint>();
            var openPQ = new PriorityQueueB<SearchNode>();
            // Add startpoint to PQ
            openPQ.Push(new SearchNode(null, startPoint, 0));
            SearchNode currentNode;
            do
            {
                // Get the node with the smallest estimated path cost
                currentNode = openPQ.Pop();
                // End loop if we have reached end Position (our destination)
                if (currentNode.Position.Equals(endPoint))
                    break;

                // Get the current node's Position Connections 
                IEnumerable<TPoint> connection = ConnectionsOf(currentNode.Position);
            
                // Add connections to the open points and do not considered positions
                // we have already considered once before
                foreach (TPoint p in connection.Where(t => !closedSet.Contains(t)))
                {
                    // Past path cost from start node to new node through the current node
                    double g;
                    if (currentNode.Parent != null)
                        g = CostBetween(currentNode.Position, p) +
                            CostBetween(currentNode.Parent.Position, currentNode.Position);
                    //if the current node's parent is null then this is the start node and there is no past path cost
                    else
                        g = CostBetween(currentNode.Position, p);
                    
                    // Estimated future path cost from new node to end node
                    double h = Heuristic(p, endPoint);

                    openPQ.Push(new SearchNode(currentNode, p, h + g));
                    
                }
                // Add this Position to the Set of positions that have already been considered once
                closedSet.Add(currentNode.Position);

                // Fail check
                //++iter;
                //if (FailState())
                //    FailAction();

            } while (openPQ.Peek() != null);

            #endregion Core of A* algorithm

            // recurse backwards over the nodes and their parents to find the solution path
            solution.Push(currentNode.Position);
            while (currentNode.Parent != null)
            {
                currentNode = currentNode.Parent;
                solution.Push(currentNode.Position);
            }
        

            return solution;
        }

        // IsSolvable should be overridden if question of solvability is much more efficient than A* algorithm
        // Allows for an additional function that tells whether the problem is solvable
        // and if it is not, FindPathBetween will throw an ArgumentException.
        virtual protected bool IsSolvable(TPoint startPoint, TPoint endPoint)
        {
            return true; //TODO maybe make abstract
        }

        protected virtual bool FailState() {
            
            return false;
        }

        protected virtual IEnumerable<TPoint> FailAction() { 
                return new Stack<TPoint>();
        }
        #endregion Methods

        #region Abstract methods
        // Heuristic function h(x,y)
        protected abstract double Heuristic(TPoint p1, TPoint p2);

        // ConnectionsOf gives the points the player/npc/monster can move to from its current position, p.
        // Letting the programmer choose this allows behaviour such as a Knights in Chess //TODO check if true :D
        // or allows for a hexagonal map etc. Connections are usually called neighbours in other implementations.
        protected abstract IEnumerable<TPoint> ConnectionsOf(TPoint p);

        protected abstract int CostBetween(TPoint from, TPoint to);

        #endregion Abstract methods

        #region Nested class
        // Node used in searching for best path
        private class SearchNode : IComparable<SearchNode>
        {
            public SearchNode Parent { get; private set; }      // The previous node on the path that lead to this node
            private readonly double _estimatedPathCost;   // Total movement cost to this node + Heuristic from this node to end node
            public TPoint Position { get; private set; }           // Position associated with this node
            public int Moves // Keeps track of how many moves have been made 
            {
                get
                {
                    return Parent != null ? Parent.Moves + 1 : 0;
                }
            }

            public SearchNode(SearchNode parent, TPoint position, double estimatedPathCost)
            {
                Parent = parent;
                Position = position;
                _estimatedPathCost = estimatedPathCost;
            }

            public int CompareTo(SearchNode that)
            {
                return Math.Sign(_estimatedPathCost - that._estimatedPathCost);
            }
        }

        #endregion Nested class

    }
}

namespace Assets.Scripts.Framework.Examples {
    // Example classes for AStarAlgorithm
    public class Point : IPoint
    {
        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }
        public int X { get; private set; }
        public int Y { get; private set; }
        public IEnumerable<Point> Connections { get; set; } // TODO could be stored in Dictionary<Point, List<Point>>
    }

    /// <summary>
    /// AStarManhattan
    /// This class also serves as an example of how a heuristic can be written that would be accepted by
    /// the AStarAlgorithm class.
    /// </summary>
    public class AStarManhattan : AStarAlgorithm<Point>
    {
        public int Multiplier { get; private set; }

        AStarManhattan()
        {
            Multiplier = 1;
        }

        AStarManhattan(int multiplier)
        {
            Multiplier = multiplier;
        }

        protected override double Heuristic(Point p1, Point p2)
        {
            return (Math.Abs(p1.X - p2.X) + Math.Abs(p1.Y - p2.Y)) * Multiplier;
        }

        protected override IEnumerable<Point> ConnectionsOf(Point p)
        {
            return p.Connections;
        }

        protected override int CostBetween(Point from, Point to)
        {
            return 1;
        }
    }
}