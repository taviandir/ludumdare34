﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class RotateAroundSelf : MonoBehaviour 
{
    public bool reverse = false;
    public float rotateSpeed = 3.0f;

    private Vector3 dir;

    void Start()
    {
    }

    void Update()
    {
        if (reverse)
            dir = -Vector3.forward;
        else
            dir = Vector3.forward;

        transform.Rotate(dir, Time.deltaTime * rotateSpeed, Space.World);
    }
}
