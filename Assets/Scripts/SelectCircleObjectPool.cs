﻿using UnityEngine;
using System.Collections.Generic;

public class SelectCircleObjectPool : MonoBehaviour
{
    public GameObject objectPrefab;
    public List<SelectedCircle> pooledObjects = new List<SelectedCircle>();
    public int initialPooledAmount = 10;
    public bool doesExpand = true;
         
	void Start()
    {
        if (pooledObjects.Count == 0)
            pooledObjects.AddRange(GetComponentsInChildren<SelectedCircle>());

        foreach (var obj in pooledObjects)
            obj.gameObject.SetActive(false);
	}

    public SelectedCircle GetObject()
    {
        foreach (var obj in pooledObjects)
        {
            if (!obj.enabled) // not enabled = not being used
                return obj;
        }

        // no unused was being found, ie all are being used up, create new ones?
        if (doesExpand)
        {
            return CreateNewInstance();
        }
        return null; // if none found and cannot expand, return null
    }

    private SelectedCircle CreateNewInstance()
    {
        var go = (GameObject)Instantiate(objectPrefab, Vector3.zero, Quaternion.identity);
        var sc = go.GetComponent<SelectedCircle>();
        pooledObjects.Add(sc);
        return sc;
    }
}
