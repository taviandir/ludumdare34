﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Units;

public class ActorManager : MonoBehaviour
{
    public List<Unit> Units = new List<Unit>();
    public List<Structure> FriendlyStructures = new List<Structure>();
    public List<Structure> Enemies = new List<Structure>();

    public Transform UnitsParent;
    public Transform StructureParent;
    public Transform EnemyParent;

    #region Singleton
    private static ActorManager _instance;
    public static ActorManager Instance
    {
        get
        {
            return _instance;
        }
    }
    #endregion Singleton

    void Awake()
    {
        _instance = this;
    }

    void Start()
    {
        
        Units.AddRange(UnitsParent.GetComponentsInChildren<Unit>());
        TileMaker TileMaker = TileMaker.Instance;
        foreach (Unit unit in Units)
        {
            unit.Occupies.Add(TileMaker.Instance.GetTileNodeAtCoordinate(TileMaker.GetCoordinatesFromPosition(unit.Position)));
        }
        FriendlyStructures.AddRange(StructureParent.GetComponentsInChildren<Structure>());
        Enemies.AddRange(EnemyParent.GetComponentsInChildren<Structure>());
    }

    public void AddUnit(Unit unit)
    {
        Units.Add(unit);
    }

    public void RemoveUnit(Unit unit)
    {
        Units.Remove(unit);
    }
}
