﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Framework;
using Assets.Scripts.Units;
using UnityEngine;

[System.Serializable]
public class TileNode : MonoBehaviour, IPoint
{
    public int xCoord = 0;
    public int yCoord = 0;
    public Vector2 Position
    {
        get { return new Vector2(transform.position.x, transform.position.y); }
    }
    public bool isWalkable = true;
    public Structure occupiedBy;
    [SerializeField] public List<TileNode> Connections;

    #region IPoint

    public int X
    {
        get { return xCoord; }
    }
    public int Y
    {
        get { return yCoord; }
    }
    #endregion IPoint

    public void SetCoords(TileCoord coord)
    {
        SetCoords(coord.X, coord.Y);
    }

    public void SetCoords(int x, int y)
    {
        xCoord = x;
        yCoord = y;
    }

    public void AddConnection(TileNode node)
    {
        //Debug.Log("adding connections");
        if (Connections == null) 
            Connections = new List<TileNode>();
        if (!Connections.Contains(node)) // prevent duplicates
        {
            Connections.Add(node);
        }
    }

    public void ClearConnections()
    {
        if (Connections != null)
            Connections.Clear();
        //Connections = new List<TileNode>();
    }

    
}

public class TileCoord
{
    public TileCoord(int x, int y)
    {
        X = x;
        Y = y;
    }

    public int X { get; private set; }
    public int Y { get; private set; }

    public override string ToString()
    {
        return String.Format("[{0},{1}]", X, Y);
    }
}