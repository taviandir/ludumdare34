﻿using System.Collections.Generic;
using UnityEngine;

public class TileSet : ScriptableObject
{
    public List<TileNode> prefabs = new List<TileNode>();
}
