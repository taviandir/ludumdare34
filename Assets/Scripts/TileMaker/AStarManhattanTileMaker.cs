﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Framework;
using UnityEngine;

public class AStarManhattanTileMaker : AStarAlgorithm<TileNode>
{
    public int Multiplier { get; private set; }

    public AStarManhattanTileMaker()
    {
        Multiplier = 1;
    }

    public AStarManhattanTileMaker(int multiplier)
    {
        Multiplier = multiplier;
    }

    protected override double Heuristic(TileNode p1, TileNode p2)
    {
        return (Math.Abs(p1.X - p2.X) + Math.Abs(p1.Y - p2.Y)) * Multiplier;
    }

    protected override IEnumerable<TileNode> ConnectionsOf(TileNode p)
    {
        return p.Connections.Where(n => n.isWalkable); // filter to prevent walking thru buildings
    }

    protected override int CostBetween(TileNode from, TileNode to)
    {
        return 1;
    }

    protected override bool IsSolvable(TileNode from, TileNode to)
    {
        if (!to.isWalkable)
            return false;
        return true;
    }

    protected override bool FailState()
    {
        Debug.Log("Failstate");
        
        return iter > 1000;
    }
    protected override IEnumerable<TileNode> FailAction()
    {
        return new List<TileNode>();
    }
}
