﻿using UnityEngine;
using System.Collections.Generic;
using Assets.Scripts.Units;

public class TileStructureSet : ScriptableObject
{
    public List<Structure> prefabs = new List<Structure>();
}
