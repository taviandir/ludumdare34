﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using Assets.Scripts.Units;

public class TileMaker : MonoBehaviour
{
    public bool cornerCutting = false; // pathfinding
    public float width = 64.0f;
    public float height = 64.0f;
    public Transform selectedPrefab;
    public TileSet tileSet;
    public TileStructureSet structureSet;
    public List<Structure> structures = new List<Structure>();

    // TileMakerEditor variables
    public bool drawGrid = true;
    public bool drawConnections = true;
    public Color gridColor = Color.green;
    public Color connectionColor = Color.red;
    public int tileSelectionIndex;
    public int structureSelectionIndex;
    public int layerSelectionIndex;
    public TileLayer currentLayer;

    private readonly AStarManhattanTileMaker _pathfinder = new AStarManhattanTileMaker();

    #region Singleton
    private static TileMaker _instance;
    public static TileMaker Instance
    {
        get
        {
            return _instance;
        }
    }
    #endregion Singleton

    void Awake()
    {
        _instance = this;
    }

    void OnDrawGizmos()
    {
        Vector3 pos = Camera.current.transform.position;

        if (drawGrid)
        {
            Gizmos.color = gridColor;
            // vertical lines
            for (float y = pos.y - 800.0f; y < pos.y + 800.0f; y += height)
            {
                Gizmos.DrawLine(new Vector3(-100000f, Mathf.Floor(y/height)*height, 0),
                    new Vector3(100000f, Mathf.Floor(y/height)*height, 0));
            }
            // horizontal lines
            for (float x = pos.x - 1200.0f; x < pos.x + 1200.0f; x += width)
            {
                Gizmos.DrawLine(new Vector3(Mathf.Floor(x/width)*width, -100000f, 0),
                    new Vector3(Mathf.Floor(x/width)*width, 100000f, 0));
            }
        }
        if (drawConnections)
        {
            Gizmos.color = connectionColor;
            int i = 0;
            while (i < transform.childCount)
            {
                Transform t = transform.GetChild(i++);
                TileNode node = t.GetComponent<TileNode>();
                if (node == null || node.Connections == null) continue;
                foreach (TileNode conn in node.Connections)
                {
                    if (conn == null)
                    {
                        Debug.Log("connection null:" + node.xCoord + "," + node.yCoord);
                        continue;
                    }
                    if (conn.X > node.X || conn.Y > node.Y)
                    {
                        Gizmos.DrawLine(node.transform.position, conn.transform.position);
                    }
                }
            }
        }
    }

    public IEnumerable<TileNode> CalculatePath(TileNode from, TileNode to)
    {
        return _pathfinder.FindPathBetween(from, to);
    }

    public TileNode GetTileNodeAtCoordinate(TileCoord coord)
    {
        return GetTileNodeAtCoordinate(coord.X, coord.Y);
    }

    public TileNode GetTileNodeAtCoordinate(int x, int y)
    {
        int i = 0;
        while (i < transform.childCount)
        {
            Transform t = transform.GetChild(i++);
            var tileNode = t.GetComponent<TileNode>();
            if (tileNode != null && tileNode.X == x && tileNode.Y == y)
                return tileNode;
        }
        Debug.Log("[TM] GetTileNodeAtCoordinate(): returning null");
        return null;
    }

    public TileCoord GetCoordinatesFromPosition(Vector2 position)
    {
        return new TileCoord(Mathf.FloorToInt(position.x / width), Mathf.FloorToInt(position.y / height));
    }

    public void SetBuiltTile(Structure str, List<TileNode> affectedNodes)
    {
        foreach (var n in affectedNodes)
        {
            n.occupiedBy = str;
            n.isWalkable = false;
        }
    }

    public void UnsetBuiltTile(List<TileNode> affectedNodes)
    {
        foreach (var n in affectedNodes)
        {
            n.occupiedBy = null;
            n.isWalkable = true;
        }
    }
}

public enum TileLayer
{
    Map = 0,
    Structures = 1,
}