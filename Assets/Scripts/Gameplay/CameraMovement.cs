﻿using UnityEngine;
using System.Collections.Generic;

public class CameraMovement : MonoBehaviour
{
    [Range(0.0f, 0.4f)] public float topMove       = 0.05f;
    [Range(0.0f, 0.4f)] public float bottomMove    = 0.05f;
    [Range(0.0f, 0.4f)] public float leftMove      = 0.05f;
    [Range(0.0f, 0.4f)] public float rightMove     = 0.05f;
    public float horizontalSpeed = 4.0f;
    public float verticalSpeed = 4.0f;
    public float xMinMoveLimit = -50f;
    public float xMaxMoveLimit = 50f;
    public float yMinMoveLimit = -50f;
    public float yMaxMoveLimit = 50f;

    void Update()
	{
	    Vector3 vpPos = Input.mousePosition.ScreenToViewPoint();
        Vector3 direction = Vector3.zero;
	    Vector3 movement = new Vector3(horizontalSpeed, verticalSpeed, 0f);

        // check top
        if (vpPos.y > 1.0f - topMove)
	    {
	        direction.y = 1;
	    }
        // check bottom
	    else if (vpPos.y < bottomMove)
	    {
	        direction.y = -1;
	    }

        // check left
	    if (vpPos.x < leftMove)
	    {
	        direction.x = -1;
	    }
        // check right
	    else if (vpPos.x > 1.0f - rightMove)
	    {
	        direction.x = 1;
	    }

        // double speed if also holding down arrow button
        if (Input.GetKey(KeyCode.DownArrow))
        {
            direction.y -= 1;
        }
        else if (Input.GetKey(KeyCode.UpArrow))
        {
            direction.y += 1;
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            direction.x -= 1;
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            direction.x += 1;
        }

        Vector3 newPos = transform.position +
                             Vector3.Scale(direction, movement) * Time.deltaTime;
        // clamp movement
        newPos.x = Mathf.Clamp(newPos.x, xMinMoveLimit, xMaxMoveLimit);
        newPos.y = Mathf.Clamp(newPos.y, yMinMoveLimit, yMaxMoveLimit);
        transform.position = newPos;
	}
}