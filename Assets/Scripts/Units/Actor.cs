﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Units
{
    public class Actor : MonoBehaviour
    {
        public string Name;
        public string Description;
        public int SizeX;
        public int SizeY;
        public int HitPoints;
        public int BuildingTime;
        
        // After being built
        public int CurrentHP;
        public List<TileNode> Occupies;

        public static readonly Actor Empty = null;
        // building UI img

        // Animation
        void Start()
        {
            Occupies = new List<TileNode>();
        }
        // Death effect .. death animation, remove from map, etc.
        void Death()
        {
            foreach (TileNode tn in Occupies)
            {
                // tn.OccupiedBy = Empty;
            }
        }
    }
}
