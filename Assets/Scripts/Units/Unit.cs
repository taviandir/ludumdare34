﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

namespace Assets.Scripts.Units
{
    public class Unit : Actor
    {
        public int Range;
        public int MoveSpeed;

        public Vector2 Position
        {
            get { return new Vector2(transform.position.x, transform.position.y); }
            set { transform.position = new Vector3(value.x, value.y, transform.position.z); }
        }
        private AStarManhattanTileMaker _pathFinding;
        private Queue<TileNode> _path;
        private bool _betweenNodes = false;
        private TileNode _currentNodeInPath = null;
        private float _startTimeMoveToNode = 0;
        private Structure _target = null;

        void Start()
        {
             _pathFinding = new AStarManhattanTileMaker();
            CurrentHP = HitPoints;
            _path = new Queue<TileNode>();
        }

        public void OrderAttack(Structure enemyStructure) //rename
        {
            FindMinPath(Occupies, enemyStructure.Occupies);
            StopMovement();
            StopAttack();
            StartCoroutine("StartAttackWhenInRange");
        }

        IEnumerator StartAttackWhenInRange()
        {
            TileNode inRange;
            while ((inRange = InRangeOf(_target.Occupies)) == null)
                yield return null;

            StopMovement();
            StartCoroutine("Attack", inRange);
        }

        IEnumerator Attack(TileNode targetsClosestNode)
        {
            //Fire at ClosestNode
            //End when enemy dead
            yield return null;
        }

        IEnumerator SearchForAutoAttackTarget()
        {
            //Search range radius around node when standing still
            yield return null;
        }

        void StopAttack()
        {
            StopCoroutine("Attack");
        }

        private TileNode InRangeOf(IEnumerable<TileNode> area)
        {
            var minDist = float.PositiveInfinity;
            var minDistNode = new TileNode();

            foreach (TileNode tnActor in Occupies)
            {
                foreach (TileNode tnArea in area)
                {
                    float dist = (float) MathUtilities.ManhattanDistance(tnActor, tnArea);
                    if (minDist > dist)
                    {
                        minDist = dist;
                        minDistNode = tnArea;
                    }
                }
            }
            if (Range >= minDist)
                return minDistNode;

            return null;
        }

        public IEnumerable<TileNode> FindMinPath(IEnumerable<TileNode> area, TileNode node)
        {
            var q = new Queue<TileNode>();
            var q2 = new Queue<TileNode>(area);
            var q3 = q2.Dequeue();

            q.Enqueue(node);
            try {
                return _pathFinding.FindPathBetween(q3, node);
            }
            catch (System.ArgumentException) { 
                return new Queue<TileNode>();
            }
                //return FindMinPath(area, q);
        }

        public IEnumerable<TileNode> FindMinPath(IEnumerable<TileNode> area1, IEnumerable<TileNode> area2)
        {
            var minPath = new Stack<TileNode>();

            foreach (TileNode tna in area1) {
                foreach (TileNode tnb in area2)
                {
                    
                    var path = (Stack<TileNode>) _pathFinding.FindPathBetween(tna, tnb);
                    if (minPath.Count == 0 || minPath.Count > path.Count)
                        minPath = path;
                }
            }

            return minPath;
        }

        public void StopMovement()
        {
            StopCoroutine("MoveAlongAssignedPath");
        }

        public void OrderMoveTo(TileNode node)
        {
            var path = FindMinPath(Occupies, node);
            MoveViaPath(path);
        }

        public void MoveViaPath(IEnumerable<TileNode> newPath)
        {
            _path = new Queue<TileNode>(newPath);
            StopMovement();
            StartCoroutine("MoveAlongAssignedPath");
        }
        
        public IEnumerator MoveAlongAssignedPath()
        {
            while (_path.Count != 0)
            {
                // Checks if unit is between two nodes, if so, do not start moving towards the 
                // next node.

                // Even if someone stops a previous run of this coroutine and starts a new one
                // the last executed MoveOneNode will not be stopped, this is the reason why 
                // MoveOneNode has a separate coroutine.
                if (!_betweenNodes)
                {
                    TileNode nextPathNode = _path.Dequeue();
                    Occupies[0] = nextPathNode;
                    StartCoroutine("MoveToNode", nextPathNode);
                }
                yield return null;
            }

        }

        // Moves smoothly between two nodes at MoveSpeed
        public IEnumerator MoveToNode(TileNode node)
        {
            if (!_betweenNodes)
                _startTimeMoveToNode = Time.time;

            while (Position != node.Position) { 
                _betweenNodes = true;
                Position = Vector2.Lerp(Position, node.Position, MoveSpeed*(Time.time - _startTimeMoveToNode));
                yield return null;
            }

            _betweenNodes = false;
        }

        public void Death()
        {
            StopAllCoroutines();
        }

        public void ReceiveDamage(int damage)
        {
            CurrentHP -= damage;
            if (CurrentHP <= 0)
                Death();
        }
    }
}
