﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Units;

public class TotemPole : Structure
{
    public EnemyRangeTrigger RangeTrigger;
    public LineRenderer LineRenderer;
    public Transform LaserStart;
    public float Range = 1.6f;
    public float AttackInterval = 2.5f;
    public int Damage = 1;
    public float LaserDuration = 0.25f;

    private float _timeLastAttack = -10f;

    void Start()
    {
        RangeTrigger.GetComponent<CircleCollider2D>().radius = Range;
    }

    void Update()
    {
        if (Time.time >= _timeLastAttack + AttackInterval)
        {
            // ready to attack
            if (RangeTrigger.UnitsInRange.Count > 0)
            {
                // Attack priority: first in list with hp > 0
                Unit target = RangeTrigger.UnitsInRange.FirstOrDefault(u => u.CurrentHP > 0);
                if (target != null)
                {
                    Attack(target);
                }
            }
        }
    }

    private void Attack(Unit target)
    {
        target.ReceiveDamage(Damage); // deal damage
        _timeLastAttack = Time.time; // reset attack timer

        // use line renderer to show attack
        StartCoroutine(DoLaserAttack(target.transform.pos()));
    }

    IEnumerator DoLaserAttack(Vector3 endPosition)
    {
        LineRenderer.enabled = true;
        LineRenderer.useWorldSpace = true;
        LineRenderer.SetVertexCount(2);
        LineRenderer.SetPosition(0, LaserStart.pos() + Vector3.back);
        LineRenderer.SetPosition(1, endPosition + Vector3.back);

        yield return new WaitForSeconds(LaserDuration);

        LineRenderer.enabled = false;
    }
}
