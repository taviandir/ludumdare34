﻿using UnityEngine;
using System.Collections.Generic;
using Assets.Scripts.Units;

public class EnemyRangeTrigger : MonoBehaviour
{
    public List<Unit> UnitsInRange = new List<Unit>(); 

    void OnTriggerEnter2D(Collider2D col)
    {
        var u = col.GetComponent<Unit>();

        if (u != null)
            UnitsInRange.Add(u);
    }

    void OnTriggerExit2D(Collider2D col)
    {
        var u = col.GetComponent<Unit>();

        if (u != null)
            UnitsInRange.Remove(u);
    }
}
