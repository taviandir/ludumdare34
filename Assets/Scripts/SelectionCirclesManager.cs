﻿using UnityEngine;
using System.Collections.Generic;
using Assets.Scripts.Units;

public class SelectionCirclesManager : MonoBehaviour
{
    public SelectCircleObjectPool objectPool;

    private SelectionManager _sm;
    private List<SelectedCircle> _selectedCircles = new List<SelectedCircle>(); 

    void Start()
    {
        _sm = SelectionManager.Instance;
    }

    void LateUpdate()
    {
        // deselect all circles from last frame
        foreach (var sc in _selectedCircles)
            sc.UnsetCircle();

        _selectedCircles.Clear();

        // get selected units
        List<Unit> selectedUnits = _sm.SelectedUnits;
        foreach (var unit in selectedUnits)
        {
            Vector3 pos = unit.pos();
            var circle = objectPool.GetObject();
            if (circle != null)
            { 
                _selectedCircles.Add(circle);
                circle.SetCircle(pos);
            }
        }
    }
}
