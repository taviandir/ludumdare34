﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Units;

public class SelectionManager : MonoBehaviour
{
    public static Vector2 MousePositionToWorldPoint
    {
        get
        {
            return new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
        }
    }
    //ActorSelection variables
    public readonly float CamRayLength = 100f;
    private bool _isDragging;
    private Vector2 _dragStartPosition;
    public List<Unit> SelectedUnits;
    public string Mouse1 = "Fire1";
    public string Mouse2 = "Fire2";

    //StructurePlacement variables
    public Color CanPlaceColor;
    public Color CannotPlaceColor;
    public GameObject PreviewStructure;
    public GameObject PreviewStructurePrefab;
    public SelectionState CurrentState = SelectionState.None;

    private TileMaker TileMaker;
    private SpriteRenderer SpriteRenderer;

    #region Singleton
    private static SelectionManager _instance;
    public static SelectionManager Instance
    {
        get
        {
            return _instance;
        }
    }
    #endregion Singleton

    void Awake()
    {
        _instance = this;
    }

    void Start()
    {
        TileMaker = TileMaker.Instance;
        if (PreviewStructure != null)
            SpriteRenderer = PreviewStructure.GetComponent<SpriteRenderer>();

        //ActorManager.Instance.Units.Add(GameObject.FindGameObjectWithTag("Unit").GetComponent<Unit>()); //TODO TEST remove remove
        //Unit mrBond = ActorManager.Instance.Units[0];
        //mrBond.Occupies.Add(TileMaker.Instance.GetTileNodeAtCoordinate(GetCoordinatesFromMousePosition(mrBond.transform.position)));
    }

    void Update()
    {
        ActorSelection();
        StructurePlacement();
    }

    private void ActorSelection()
    {
        // TODO need UI collision that cancels all this
        // Mouse button 1 drag select
        if (Input.GetButtonDown(Mouse1))
        {
            // 
            if (!_isDragging)
            {
                // Cancel current selection
                SelectedUnits = new List<Unit>();
                // Start drag
                _dragStartPosition = MousePositionToWorldPoint;
                _isDragging = true;
            }
        }
        else if (Input.GetButtonUp(Mouse1))
        {
            _isDragging = false;
            foreach (Unit unit in ActorManager.Instance.Units)
            {
                if (DragSelectionContains(unit.Position))
                {
                    //Debug.Log(unit.Name);
                    SelectedUnits.Add(unit);
                    
                }
            }
        }

        // Mouse button 2 Move or Attack 
        if (Input.GetButtonDown(Mouse2))
        {
            foreach (Unit unit in SelectedUnits)
            {
                GameObject mouseHitObject = MouseHit();
                TileNode hitTileNode = mouseHitObject.GetComponent<TileNode>();
                if (hitTileNode != null)
                {
                    unit.OrderMoveTo(hitTileNode);
                }
                // TODO test if occupiedBy enemy
                Structure hitStructure = mouseHitObject.GetComponent<Structure>();

            }
        }
    }

    private bool DragSelectionContains(Vector2 position)
    {
        var minVector = Vector2.Min(_dragStartPosition, MousePositionToWorldPoint);
        var maxVector = Vector2.Max(_dragStartPosition, MousePositionToWorldPoint);
        if (minVector.x <= position.x && position.x <= maxVector.x)
        {
            if (minVector.y <= position.y && position.y <= maxVector.y)
            {
                return true;
            }
        }
        return false;
    }

    private GameObject MouseHit()
    {
        // Ray cast from main camera

        
         int layerMask = 1 << 8; //TEST LAYER
        RaycastHit2D rayHit = Physics2D.Raycast(MousePositionToWorldPoint, Vector2.zero, 0f, layerMask);

        if (rayHit) // TODO check if it works without mask
        {
            GameObject sceneObjectHitByRay = rayHit.collider.gameObject;
            Vector2 sceneObjectPosition = rayHit.collider.gameObject.transform.position;
            return sceneObjectHitByRay;
            //var useable = sceneObjectHitByRay.GetComponent<IUseable>();

            //    if (useable != null)
            //        _player.MoveToThenUse(sceneObjectPosition, useable);
            //    else if (Vector3.Magnitude(rayHit.point - _player.transform.position) >= MinDeltaDestination)
            //    {
            //        _player.StopCoroutine("UseWhenPossible");
            //        _player.SetDestination(rayHit.point);
            //    }
            //}
        }
        return null;
    }

    private void StructurePlacement()
    {
        if (CurrentState == SelectionState.PlaceStructurePreview)
        {
            if (Input.GetButtonDown(Mouse2)) // right-click = CANCEL
            {
                Debug.Log("canceling structure preview");
                SetState(SelectionState.None);
                return;
            }
            PlacePreviewAligned(); // position the preview instance
            var ts = PreviewStructure.GetComponent<Structure>();
            List<TileNode> affectedNodes;
            Vector3 mp = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (CanPlaceStructure(mp, ts, out affectedNodes))
            {
                SpriteRenderer.color = CanPlaceColor;
                if (Input.GetButtonDown(Mouse1))
                {
                    PlaceStructure(mp, affectedNodes);
                    SetState(SelectionState.None);
                }
            }
            else
            {
                SpriteRenderer.color = CannotPlaceColor;
            }
        }
    }

    public void SetState(SelectionState newState)
    {
        if (newState == SelectionState.None)
        {
            PreviewStructure.SetActive(false);
        }
        else if (newState == SelectionState.PlaceStructurePreview)
        {
            PreviewStructure.SetActive(true);
        }
        else if (newState == SelectionState.Units)
        {
            PreviewStructure.SetActive(false);
        }
        CurrentState = newState;
    }

    public void SetPlaceStructurePreviewState()
    {
        SetState(SelectionState.PlaceStructurePreview);
    }

    /*
        ToDo:
        Get building selection
        building follows mouse positioning
    */

    public void PlacePreviewAligned()
    {
        Vector3 aligned = GetAlignedPosition(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        var ts = PreviewStructure.GetComponent<Structure>();

        // calculate offset
        float xMid = ts.SizeX / 2.0f;
        float yMid = ts.SizeY / 2.0f;
        float xOffsetDiff = Mathf.Abs(0.5f - xMid);
        float yOffsetDiff = Mathf.Abs(0.5f - yMid);
        float xOffset = TileMaker.width * xOffsetDiff;
        float yOffset = TileMaker.height * yOffsetDiff;

        PreviewStructure.transform.position = aligned + new Vector3(xOffset, yOffset);
    }

    private Vector3 GetAlignedPosition(Vector3 worldMousePos)
    {
        //Debug.Log("aligned:" + (Camera.current == null));
        //Ray ray =
        //    Camera.current.ScreenPointToRay(new Vector3(worldMousePosition.x,
        //        worldMousePosition.y + Camera.current.pixelHeight, 0));
        //Vector3 worldMousePos = ray.origin;
        return new Vector3(Mathf.Floor(worldMousePos.x / TileMaker.width) * TileMaker.width + TileMaker.width / 2.0f,
            Mathf.Floor(worldMousePos.y / TileMaker.height) * TileMaker.height + TileMaker.height / 2.0f, 0.0f);
    }

    private bool CanPlaceStructure(Vector3 worldMousePosition, Structure str, out List<TileNode> nodes)
    {
        nodes = new List<TileNode>(str.SizeX * str.SizeY);
        TileCoord initialCoord = TileMaker.GetCoordinatesFromPosition(MousePositionToWorldPoint);
        for (int x = 0; x < str.SizeX; ++x)
        {
            for (int y = 0; y < str.SizeY; ++y)
            {
                var node = TileMaker.GetTileNodeAtCoordinate(initialCoord.X + x, initialCoord.Y + y);
                nodes.Add(node);
            }
        }

        return nodes.All(n => n != null && n.isWalkable && n.occupiedBy == null);
    }

    private GameObject PlaceStructure(Vector3 worldMousePosition, List<TileNode> affectedNodes)
    {
        GameObject go = (GameObject)Instantiate(PreviewStructurePrefab);
        Vector3 aligned = GetAlignedPosition(worldMousePosition);
        var ts = go.GetComponent<Structure>();

        // calculate offset
        float xMid = ts.SizeX / 2.0f;
        float yMid = ts.SizeY / 2.0f;
        float xOffsetDiff = Mathf.Abs(0.5f - xMid);
        float yOffsetDiff = Mathf.Abs(0.5f - yMid);
        float xOffset = TileMaker.width * xOffsetDiff;
        float yOffset = TileMaker.height * yOffsetDiff;

        // set position
        go.transform.position = aligned + new Vector3(xOffset, yOffset);
        // ToDo : set occupiedBy under a parent
        //go.transform.parent = tileMaker.transform;

        TileMaker.SetBuiltTile(ts, affectedNodes);
        ts.Occupies = affectedNodes; // save which nodes it is saved upon
        return go;
    }
}

public enum SelectionState
{
    None,
    PlaceStructurePreview,
    Units,
}
