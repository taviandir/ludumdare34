﻿using UnityEngine;
using System.Collections;

public class MainMenuScript : MonoBehaviour 
{
	public GameObject mainMenuPanel;
	public GameObject instructionsPanel;
	
	public void StartNewGame()
	{
		Application.LoadLevel("LOAD_FIRST_LEVEL");
	}
	
	public void ShowInstructions()
	{
		mainMenuPanel.SetActive(false);
		instructionsPanel.SetActive(true);
	}
	
	public void BackToMainMenu()
	{
		mainMenuPanel.SetActive(true);
		instructionsPanel.SetActive(false);
	}
	
	public void QuitApplication()
	{
		Application.Quit();
	}
}
